# issue-bot

Bot to manage issues for CI pipelines failures.

[![GoDoc](https://godoc.org/gitlab.com/gitlab-org/distribution/issue-bot?status.svg)](https://godoc.org/gitlab.com/gitlab-org/distribution/issue-bot)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/gitlab-org/distribution/issue-bot)](https://goreportcard.com/report/gitlab.com/gitlab-org/distribution/issue-bot)

## Why?

Teams often need to be aware of failures in continuous integration pipelines. Typically, this is achieved by
sending notifications to a Slack channel.

While this approach works, notifications in Slack can easily be missed - especially
if multiple are sent in a short time period.

Logging these events as issues with `issue-bot` provides several benefits:

* Reduces risk of missing a pipeline failure due to heavy Slack activity
* Records actions taken to resolve a failure providing both a record of knowledge and an audit trail
* Patterns can be observed by linking failure Issues to root-cause issues

Eventually, this could be a great built-in feature of GitLab, similar to the way
[release-cli](https://gitlab.com/gitlab-org/release-cli) is integrated into GitLab
CI to generate Releases.

## How it works

1. The 'issue-bot' job is triggered (`on_failure`).
1. `issue-bot` creates an issue with the branch name and commit SHA in the title.
1. `issue-bot` fills in the issue description with a link to failed pipeline,
    related commit, and list of links to failed jobs
1. `issue-bot` labels the issue with:
   - `pipeline failure`
   - `pipeline failure::needs investigation`
   - labels listed in `ISSUE_BOT_LABELS_EXTRA` (see [Custom labels](#use-custom-labels))

If the same pipeline is retried (meaning that a pipeline ran for
the same combination of branch name and commit SHA), then `issue-bot` will
recognize that an issue already exists and will not create a duplicate issue.

If the issue exists but is closed, then it will reopen the issue.

If `ISSUE_TRACKER_PROJECT_ID` is configured, then issues will be created in the
specified project. The issue title will be prefixed with the project name where
the pipeline failure occurred. This option can be used when a project has a
custom issue tracker in a separate project.

Using [scoped labels](https://docs.gitlab.com/ee/user/project/labels.html#scoped-labels),
an issue board can be made using the `pipeline failure` labels:

- `pipeline failure::needs investigation`
- `pipeline failure::under investigation`

## Examples

For many examples of issues generated from CI, see the
[Distribution team pipeline failure board](https://gitlab.com/groups/gitlab-org/-/boards/4076077?label_name%5B%5D=pipeline%20failure&label_name%5B%5D=group%3A%3Adistribution).

## Usage

1. Create an access token with `api` scope.
   - [Project access tokens](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html) can be helpful here.
    - If using a project access token, select the `Guest` role or higher.
1. Add a project variable named `ISSUE_BOT_API_TOKEN` and paste your access token.
   Be sure it is masked.
1. Add the following snippet to your CI configuration file:

```yaml
issue-bot:
  stage: report
  image: registry.gitlab.com/gitlab-org/distribution/issue-bot:latest
  script: /issue-bot
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
      when: on_failure
```

The next time the pipeline fails, this job will run and create an issue in the project.
The logs in this job will include a link to this issue.

## Advanced

Below are instructions for advanced use cases of `issue-bot`.

### Create issues in a separate project

You can optionally tell `issue-bot` to create Issues in a separate project by
configuring the following variables:

- `ISSUE_TRACKER_PROJECT_ID`: The project ID of the project where you want Issues
  to be created (example: `12345`)

### Create issues in a separate GitLab instance

You can optionally tell `issue-bot` to create Issues on a separate instance of
GitLab by configuring the following variables:

- `ISSUE_TRACKER_PROJECT_ID`: The project ID of the project where you want Issues
  to be created (example: `12345`)
- `ISSUE_TRACKER_BASE_URL`: The base URL of the instance where Issues will be created
  (example: `https://mygitlab.com/`)
- `ISSUE_TRACKER_API_TOKEN`: The access token (with `api` scope) with access to
  create issues in the desired project (example: `abcd123`)

### Use custom labels

Override and expand default labels using environment variables:

| variable                      |                 default                 |                                                   description                                                    |
|-------------------------------|:---------------------------------------:|:----------------------------------------------------------------------------------------------------------------:|
| `ISSUE_BOT_LABEL_FAILURE`     | `pipeline failure`                      | Non-nested label used to easily filter for pipeline-failure-related issues.                                      |
| `ISSUE_BOT_LABEL_READY`       | `pipeline failure::needs investigation` | Indicates that the Issue requires investigation into the root cause of the failure.                                  |
| `ISSUE_BOT_LABEL_IN_PROGRESS` | `pipeline failure::under investigation` | Indicates that the Issue is under investigation into the root cause of the failure.                              |
| `ISSUE_BOT_LABELS_EXTRA`      |                  empty                  | Comma-separated list of additional labels to be applied to created issues. Example: `label1::foo,label2,label3`. |

#### Notes on use of labels

Note that any labels provided in `ISSUE_BOT_LABEL_FAILURE` and `ISSUE_BOT_LABELS_EXTRA`
should not be removed or changed after the issue is created. Otherwise, when `issue-bot`
checks to see if an issue already exists before creating a new one, it will not
be able to find an existing issue with the desired extra labels, and will
therefore create a new ("duplicate") issue with the proper extra labels.

For example: if setting `ISSUE_BOT_LABELS_EXTRA="abcd,foo::bar"`, then you:

- should not later remove label `abcd` from the issue
- should not later change label `foo::bar` to `foo:baz` on the issue
