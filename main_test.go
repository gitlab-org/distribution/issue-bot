package main

import (
	"fmt"
	"os"
	"reflect"
	"strconv"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/xanzy/go-gitlab"
)

const (
	prettyPrintedFailedJobs = `- [fakejob1](testgitlab.com/test/-/jobs/123)
- [fakejob2](testgitlab.com/test/-/jobs/456)
`

	apiToken         = "foo"
	ciServerURL      = "testgitlab.com"
	ciProjectID      = "1234"
	ciPipelineID     = "5678"
	ciPipelineURL    = "testgitlab.com/test/-/pipelines/123"
	ciProjectName    = "testproject"
	ciCommitShortSHA = "abc123"
	ciCommitRefName  = "testbranch"
	ciJobURL         = "testgitlab.com/test/-/jobs/123"
)

var (
	ENV_VARS = map[string]string{
		apiTokenEnvName:         apiToken,
		ciServerURLEnvName:      ciServerURL,
		ciProjectIDEnvName:      ciProjectID,
		ciPipelineIDEnvName:     ciPipelineID,
		ciPipelineURLEnvName:    ciPipelineURL,
		ciProjectNameEnvName:    ciProjectName,
		ciCommitShortSHAEnvName: ciCommitShortSHA,
		ciCommitRefNameEnvName:  ciCommitRefName,
		ciJobURLEnvName:         ciJobURL,
	}
)

func TestNewIncident(t *testing.T) {
	testCases := []struct {
		customIssueTracker          bool
		customIssueTrackerProjectID int
	}{
		{
			customIssueTracker: false,
		},
		{
			customIssueTracker:          true,
			customIssueTrackerProjectID: 111,
		},
		{
			customIssueTracker:          true,
			customIssueTrackerProjectID: 222,
		},
	}

	for _, tc := range testCases {
		if tc.customIssueTracker {
			t.Setenv(issueTrackerProjectIDEnvName, strconv.Itoa(tc.customIssueTrackerProjectID))
		}

		want := newBaselineTestIncident(t)

		if tc.customIssueTracker {
			want.issue.ProjectID = tc.customIssueTrackerProjectID
			want.issue.Title = fmt.Sprintf("[%s] %s", want.projectName, want.issue.Title)
		}

		got := newTestIncident(t)

		if !reflect.DeepEqual(got, want) {
			t.Errorf("got: \n\n%+v\n\nwanted: \n\n%+v\n", got, want)
		}
	}
}

func TestExtraLabels(t *testing.T) {
	defaultLabels := []string{labelPipelineFailure, labelScopeReady}

	testCases := []struct {
		extraLabels    string
		expectedLabels []string
	}{
		{
			extraLabels:    "",
			expectedLabels: defaultLabels,
		},
		{
			extraLabels:    "foo::bar",
			expectedLabels: append(defaultLabels, "foo::bar"),
		},
		{
			extraLabels:    "foo::bar,baz",
			expectedLabels: append(defaultLabels, "foo::bar", "baz"),
		},
	}

	for _, tc := range testCases {
		if tc.extraLabels != "" {
			t.Setenv(envLabelsExtra, tc.extraLabels)
		}

		getLabels()

		i := newTestIncident(t)

		assert.NoError(t, os.Unsetenv(envLabelsExtra))
		assert.ElementsMatch(t, i.issue.Labels, tc.expectedLabels)
	}
}

func TestInjectClients(t *testing.T) {
	i := newTestIncident(t)

	pipelineFailureClient := &gitlab.Client{}
	issueTrackerClient := pipelineFailureClient

	i.injectClients(pipelineFailureClient, issueTrackerClient)

	if i.pipelineFailureClient == nil {
		t.Error("pipeline failure client is nil")
	}

	if i.issueTrackerClient == nil {
		t.Error("issue tracker client is nil")
	}
}

func TestInjectFailedJobsList(t *testing.T) {
	i := newTestIncident(t)
	i.failedJobs = newFailedJobsList()
	i.injectFailedJobsList()

	want := prettyPrintedFailedJobs

	if !strings.Contains(i.issue.Description, want) {
		t.Errorf("wanted %s to include %s", i.issue.Description, want)
	}
}

func TestPrettyPrintJobs(t *testing.T) {
	want := prettyPrintedFailedJobs
	got := prettyPrintJobs(newFailedJobsList())

	if want != got {
		t.Errorf("wanted:\n\n%s\ngot:\n\n%s\n", want, got)
	}
}

// Support functions.

func setTestEnv(t *testing.T) {
	for key, value := range ENV_VARS {
		t.Setenv(key, value)
	}
}

func newTestIncident(t *testing.T) *incident {
	setTestEnv(t)

	i, err := newIncident()
	if err != nil {
		t.Fatalf("unable to create new incident: %v", err)
	}

	return i
}

func newFailedJobsList() []*gitlab.Job {
	return []*gitlab.Job{
		&gitlab.Job{
			Name:   "fakejob1",
			WebURL: "testgitlab.com/test/-/jobs/123",
		},
		&gitlab.Job{
			Name:   "fakejob2",
			WebURL: "testgitlab.com/test/-/jobs/456",
		},
	}
}

func newTestIncidentDescription(i incident) string {
	return fmt.Sprintf(issueDescriptionTemplate, i.pipelineID,
		i.pipelineURL, i.ref, i.commit, labelScopeInProgress, i.jobURL)
}

func newBaselineTestIncident(t *testing.T) *incident {
	projectIDInt, err := strconv.Atoi(ciProjectID)
	if err != nil {
		t.Fatalf("unable to convert ciProjectID %s to integer", ciProjectID)
	}

	pipelineIDInt, err := strconv.Atoi(ciPipelineID)
	if err != nil {
		t.Fatalf("unable to convert ciPipelineID %s to integer", ciProjectID)
	}

	i := &incident{
		issue: gitlab.Issue{
			Title:     "[CI] Pipeline on 'testbranch' failed for commit abc123",
			ProjectID: 1234,
			Labels:    append([]string{labelPipelineFailure, labelScopeReady}, labelsExtra...),
		},

		projectID:   projectIDInt,
		pipelineID:  pipelineIDInt,
		projectName: ciProjectName,
		pipelineURL: ciPipelineURL,
		commit:      ciCommitShortSHA,
		ref:         ciCommitRefName,
		jobURL:      ciJobURL,
	}

	i.issue.Description = newTestIncidentDescription(*i)

	return i
}
