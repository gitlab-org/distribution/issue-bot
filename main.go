package main

import (
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"

	"github.com/xanzy/go-gitlab"
)

var (
	labelPipelineFailure = "pipeline failure"
	labelScopeReady      = "pipeline failure::needs investigation"
	labelScopeInProgress = "pipeline failure::under investigation"
	labelsExtra          = []string{}
)

const (
	apiTokenEnvName = "ISSUE_BOT_API_TOKEN"

	issueTrackerProjectIDEnvName = "ISSUE_TRACKER_PROJECT_ID"

	issueTrackerBaseURLEnvName  = "ISSUE_TRACKER_BASE_URL"
	issueTrackerAPITokenEnvName = "ISSUE_TRACKER_API_TOKEN"

	ciServerURLEnvName      = "CI_SERVER_URL"
	ciProjectIDEnvName      = "CI_PROJECT_ID"
	ciPipelineIDEnvName     = "CI_PIPELINE_ID"
	ciPipelineURLEnvName    = "CI_PIPELINE_URL"
	ciProjectNameEnvName    = "CI_PROJECT_NAME"
	ciCommitShortSHAEnvName = "CI_COMMIT_SHORT_SHA"
	ciCommitRefNameEnvName  = "CI_COMMIT_REF_NAME"
	ciJobURLEnvName         = "CI_JOB_URL"

	envLabelPipelineFailure = "ISSUE_BOT_LABEL_FAILURE"
	envLabelScopeReady      = "ISSUE_BOT_LABEL_READY"
	envLabelScopeInProgress = "ISSUE_BOT_LABEL_IN_PROGRESS"
	envLabelsExtra          = "ISSUE_BOT_LABELS_EXTRA"

	issueTitleTemplate       = "[CI] Pipeline on '%s' failed for commit %s"
	issueDescriptionTemplate = `
## Summary

Pipeline [%d](%s) on '%s' failed for commit %s.

Failed job(s):
<failed jobs placeholder>

## Instructions

Please create a new issue (or multiple issues) and link
them to this issue.

Tip: you can create a related issue directly by clicking
the vertical ellipsis in the top right hand corner of this issue
and clicking "New related issue". See the documentation on
[managing issues](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#from-another-issue-or-incident)
for more information.

When investigation begins, update the label to ~"%s".

-----

_Created by [issue-bot](https://gitlab.com/gitlab-org/distribution/issue-bot) via [this job](%s)._
`
)

type incident struct {
	pipelineFailureClient *gitlab.Client
	issueTrackerClient    *gitlab.Client
	issue                 gitlab.Issue
	failedJobs            []*gitlab.Job

	pipelineID  int
	projectID   int
	projectName string
	pipelineURL string
	commit      string
	ref         string
	jobURL      string
}

// getLabels initializes label values based on environment variables.
func getLabels() {
	labelPipelineFailure = getEnvWithDefault(envLabelPipelineFailure, labelPipelineFailure)
	labelScopeReady = getEnvWithDefault(envLabelScopeReady, labelScopeReady)
	labelScopeInProgress = getEnvWithDefault(envLabelScopeInProgress, labelScopeInProgress)

	envLabelsExtra := os.Getenv(envLabelsExtra)
	if envLabelsExtra != "" {
		labelsExtra = strings.Split(envLabelsExtra, ",")
	}
}

func main() {
	getLabels()

	pipelineFailureClient, issueTrackerClient, err := createClients()
	if err != nil {
		log.Fatalf("unable to create GitLab client(s): %v", err)
	}

	incident, err := newIncident()
	if err != nil {
		log.Fatalf("unable to instantiate a new incident: %v", err)
	}

	incident.injectClients(pipelineFailureClient, issueTrackerClient)

	err = incident.getFailedJobs()
	if err != nil {
		log.Fatalf("unable to get failed jobs: %v", err)
	}

	incident.injectFailedJobsList()

	err = incident.createOrReopenIssue()
	if err != nil {
		log.Fatalf("problem ensuring issue exists: %v", err)
	}
}

func newGitLabClient(token, url string) (*gitlab.Client, error) {
	gl, err := gitlab.NewClient(token, gitlab.WithBaseURL(url))
	if err != nil {
		log.Fatalf("unable to create client: %v", err)
	}

	return gl, nil
}

func newPipelineFailureClient() (*gitlab.Client, error) {
	token, err := getEnv(apiTokenEnvName)
	if err != nil {
		return nil, fmt.Errorf("please configure %s environment variable (%v)", apiTokenEnvName, err)
	}

	url, err := getEnv(ciServerURLEnvName)
	if err != nil {
		return nil, fmt.Errorf("please configure %s environment variable (%v)", ciServerURLEnvName, err)
	}

	return newGitLabClient(token, url)
}

func newIssueTrackerClient() (*gitlab.Client, bool, error) {
	issueTrackerClientURL, configured := os.LookupEnv(issueTrackerBaseURLEnvName)
	if configured {
		issueTrackerClientToken, err := getEnv(issueTrackerAPITokenEnvName)
		if err != nil {
			return nil, true, fmt.Errorf("please configure %s environment variable (%v)", issueTrackerAPITokenEnvName, err)
		}

		issueTrackerClient, err := newGitLabClient(issueTrackerClientToken, issueTrackerClientURL)
		if err != nil {
			return nil, true, fmt.Errorf("unable to create a new GitLab dev client: %v", err)
		}

		return issueTrackerClient, true, nil
	}

	return nil, false, nil
}

func createClients() (pipelineFailureClient, issueTrackerClient *gitlab.Client, err error) {
	pipelineFailureClient, err = newPipelineFailureClient()
	if err != nil {
		return nil, nil, err
	}

	issueTrackerClient, issueTrackerConfigured, err := newIssueTrackerClient()
	if err != nil {
		return nil, nil, err
	}

	if !issueTrackerConfigured {
		issueTrackerClient = pipelineFailureClient
	}

	return pipelineFailureClient, issueTrackerClient, nil
}

func newIncident() (*incident, error) {
	projectID, err := strconv.Atoi(os.Getenv(ciProjectIDEnvName))
	if err != nil {
		return nil, fmt.Errorf("unable to get %s as integer: %v", ciProjectIDEnvName, err)
	}

	pipelineID, err := strconv.Atoi(os.Getenv(ciPipelineIDEnvName))
	if err != nil {
		return nil, fmt.Errorf("unable to get CI_PIPIELINE_ID as integer: %v", err)
	}

	incident := &incident{
		issue: gitlab.Issue{
			Labels: append([]string{labelPipelineFailure, labelScopeReady}, labelsExtra...),
		},

		projectID:   projectID,
		pipelineID:  pipelineID,
		projectName: os.Getenv(ciProjectNameEnvName),
		pipelineURL: os.Getenv(ciPipelineURLEnvName),
		commit:      os.Getenv(ciCommitShortSHAEnvName),
		ref:         os.Getenv(ciCommitRefNameEnvName),
		jobURL:      os.Getenv(ciJobURLEnvName),
	}

	incident.issue.Title = fmt.Sprintf(issueTitleTemplate, incident.ref, incident.commit)

	issueTrackerProjectIDString, configured := os.LookupEnv(issueTrackerProjectIDEnvName)
	if configured {
		issueTrackerProjectID, err := strconv.Atoi(issueTrackerProjectIDString)
		if err != nil {
			return nil, fmt.Errorf("unable to get %s as integer: %v", issueTrackerAPITokenEnvName, err)
		}

		log.Printf("setting issue project ID to %d", issueTrackerProjectID)
		incident.issue.ProjectID = issueTrackerProjectID

		log.Printf("setting issue title prefix to %s", incident.projectName)
		incident.issue.Title = fmt.Sprintf("[%s] %s", incident.projectName, incident.issue.Title)
	} else {
		log.Printf("ISSUE_TRACKER_PROJECT_ID not set, using %s for tracking issues", ciProjectIDEnvName)
		incident.issue.ProjectID = projectID
	}

	incident.issue.Description = fmt.Sprintf(issueDescriptionTemplate, incident.pipelineID,
		incident.pipelineURL, incident.ref, incident.commit,
		labelScopeInProgress, incident.jobURL)

	return incident, nil
}

func (i *incident) injectClients(pipelineFailureClient, issueTrackerClient *gitlab.Client) {
	i.pipelineFailureClient = pipelineFailureClient
	i.issueTrackerClient = issueTrackerClient
}

func (i *incident) injectFailedJobsList() {
	i.issue.Description = strings.Replace(i.issue.Description,
		"<failed jobs placeholder>", prettyPrintJobs(i.failedJobs), 1)
}

func (i *incident) issueExists() (bool, error) {
	opts := &gitlab.ListProjectIssuesOptions{
		Labels: append([]string{labelPipelineFailure}, labelsExtra...),
	}

	issues, _, err := i.issueTrackerClient.Issues.ListProjectIssues(i.issue.ProjectID, opts)
	if err != nil {
		return false, err
	}

	for _, issue := range issues {
		if issue.Title == i.issue.Title {
			i.issue = *issue
			return true, nil
		}
	}

	return false, nil
}

func (i incident) createIssue() (*gitlab.Issue, error) {
	opts := &gitlab.CreateIssueOptions{
		Title:       &i.issue.Title,
		Description: &i.issue.Description,
		Labels:      i.issue.Labels,
	}

	newIssue, _, err := i.issueTrackerClient.Issues.CreateIssue(i.issue.ProjectID, opts)
	if err != nil {
		return nil, err
	}

	return newIssue, nil
}

func (i incident) reopenIssue() error {
	reopenEvent := "reopen"
	opts := &gitlab.UpdateIssueOptions{
		StateEvent: &reopenEvent,
	}

	_, _, err := i.issueTrackerClient.Issues.UpdateIssue(i.issue.ProjectID, i.issue.IID, opts)
	if err != nil {
		return fmt.Errorf("unable to reopen issue: %v", err)
	}

	return nil
}

func (i incident) addComment(comment string) error {
	opts := &gitlab.CreateIssueDiscussionOptions{
		Body: &comment,
	}

	_, _, err := i.issueTrackerClient.Discussions.CreateIssueDiscussion(i.projectID, i.issue.IID, opts)
	if err != nil {
		return fmt.Errorf("unable to add comment on issue: %v", err)
	}

	return nil
}

func (i incident) createOrReopenIssue() error {
	exists, err := i.issueExists()
	if err != nil {
		log.Fatalf("unable to check for existing issue: %v", err)
	}

	if exists {
		switch i.issue.State {
		case "opened":
			log.Println("open issue already exists:", i.issue.WebURL)
			return nil
		case "closed":
			log.Println("issue is closed, reopening...")
			err = i.reopenIssue()
			if err != nil {
				return err
			}
			log.Println("issue reopened:", i.issue.WebURL)

			log.Println("commenting on reopened issue with link to pipeline...")
			comment := fmt.Sprintf("Reopened by pipeline [%d](%s).", i.pipelineID, i.pipelineURL)
			err = i.addComment(comment)
			if err != nil {
				return err
			}
			log.Println("comment created")

			return nil
		default:
			return fmt.Errorf("invalid error state, must be 'opened' or 'closed': %v", i.issue.State)
		}
	}

	log.Println("issue does not exist yet. Creating...")
	var newIssue *gitlab.Issue
	newIssue, err = i.createIssue()
	if err != nil {
		log.Fatalf("unable to create issue: %v", err)
	}
	log.Println("issue created:", newIssue.WebURL)

	return nil
}

func (i *incident) getFailedJobs() error {
	opts := &gitlab.ListJobsOptions{
		Scope:          []gitlab.BuildStateValue{gitlab.Failed},
		IncludeRetried: false,
	}

	jobs, _, err := i.pipelineFailureClient.Jobs.ListPipelineJobs(i.projectID, i.pipelineID, opts)
	if err != nil {
		return fmt.Errorf("unable to list pipeline jobs: %v", err)
	}

	i.failedJobs = jobs

	return nil
}

func prettyPrintJobs(jobs []*gitlab.Job) string {
	var output strings.Builder
	for _, job := range jobs {
		output.WriteString(fmt.Sprintf("- [%s](%s)\n", job.Name, job.WebURL))
	}

	return output.String()
}

func getEnv(key string) (string, error) {
	value, exists := os.LookupEnv(key)
	if !exists {
		return "", fmt.Errorf("unable to find environment variable: %s", key)
	}
	return value, nil
}

func getEnvWithDefault(key string, defaultValue string) string {
	value, exists := os.LookupEnv(key)
	if !exists {
		return defaultValue
	}
	return value
}
