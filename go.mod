module gitlab.com/gitlab-org/distribution/issue-bot

go 1.16

require (
	github.com/stretchr/testify v1.8.1
	github.com/xanzy/go-gitlab v0.50.1
)
